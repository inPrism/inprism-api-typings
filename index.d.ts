declare namespace InPrism {    
    export interface User {
        id: string,
        accountCreationDate: Date,
        created: string[],
        boosters: number,
        email: string,
        interested: string[],
        language: "fr" | "de" | "en",
        name: string,
        participated: string[],
        premiumExpDate: Date,
        profilePicture: string,
        subscribed: boolean,
        tags: string[],
        type?: "Man" | "Woman" | "Society"
    }
    
    export interface Event {
        id: string,
        author: string,
        beginningDate: Date,
        categories: string[],
        creationDate: Date,
        description: string,
        distance: number,
        endDate: Date,
        exclusiveAdvantage?: string,
        handicapAccess: boolean,
        location: {
            address: string,
            geoCoords: number[]
        },
        note?: string,
        minimumAge: number,
        minimumPrice: number,
        participate: boolean,
        participantsCount?: number,
        interested?: boolean,
        interestedCount?: number,
        picture: string,
        score: number,
        tags: string[],
        title: string
        website: string
    }
}
